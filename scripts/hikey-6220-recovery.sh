#!/bin/bash

set -e

while getopts ":s:l:p:f:n:r:x" opt; do
  case $opt in
    s)
      # script, e.g. hisi-idt.py
      SCRIPT=${OPTARG}
      ;;
    l)
      # loader, e.g. l-loader.bin
      LOADER=${OPTARG}
      ;;
    p)
      # ptable, e.g. ptable-linux-8g.img
      PTABLE=${OPTARG}
      ;;
    f)
      # fastboot, e.g. fip.bin
      FASTBOOT=${OPTARG}
      ;;
    n)
      # nvme, e.g. nvme.img
      NVME=${OPTARG}
      ;;
    x)
      # test mode
      EXIT="true"
      ;;
    r)
      # recovery.bin
      RECOVERY=${OPTARG}
      ;;
    ?)
      echo "Usage:"
      echo "-s - recovery mode script, e.g. hisi-idt.py"
      echo "-l - l-loader image, e.g. l-loader.bin"
      echo "-r - recovery image, e.g. recovery.bin"
      echo "-p - ptable, e.g. ptable-linux-8g.img"
      echo "-f - fastboot image, e.g. fip.bin"
      echo "-n - NVME image, e.g. nvme.img"
      echo "-x - print options and exit"
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

command(){
    if [ -n "$(which lava-test-case || true)" ]; then
        echo $2
        $2 && lava-test-case "$1" --result pass || lava-test-raise "$1"
    else
        echo $2
        $2
    fi
}

echo "SCRIPT" ${SCRIPT}
echo "LOADER" ${LOADER}
echo "PTABLE" ${PTABLE}
echo "FASTBOOT" ${FASTBOOT}
echo "NVME" ${NVME}
echo "RECOVERY" ${RECOVERY}
command 'LAVA_Check' true

if [ -n "${EXIT}" ]; then
  echo "Exiting as requested"
  exit
fi
ls /dev/
while [ ! -c /dev/ttyUSB* ]; do
    sleep 1
    ls /dev/ttyUSB* 2>/dev/null || echo "Waiting for /dev/ttyUSB*"
done
ln -s `find /dev/ -xdev -name "ttyUSB*" -type c -print -quit` /dev/recovery
command 'recovery_device' 'ls -l /dev/recovery'
if [ -n "${RECOVERY}" ]; then
	echo "Using ${RECOVERY} image for newer builds"
	command 'hisi-idt-l-loader' "python ${SCRIPT} --img1=${RECOVERY} -d /dev/recovery"
else
	command 'hisi-idt-l-loader' "python ${SCRIPT} --img1=${LOADER} -d /dev/recovery"
fi
echo "fastboot should wait for the device to reset here"
echo "udev rule copes with adding it to the LXC once it appears"
command 'fastboot-flash-ptable' "timeout 2m fastboot flash ptable ${PTABLE}"
if [ -n "${RECOVERY}" ]; then
	command 'fastboot-flash-loader' "fastboot flash loader ${LOADER}"
fi
command 'fastboot-flash-fastboot' "fastboot flash fastboot ${FASTBOOT}"
command 'fastboot-flash-nvme' "fastboot flash nvme ${NVME}"
echo "next boot action needs to takes care of exiting from recovery mode"
