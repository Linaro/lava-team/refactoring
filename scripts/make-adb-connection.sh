#!/bin/sh

set -e
set -x

LAVA='echo # '
if [ -n `which lava-test-case || true` ]; then
    LAVA='lava-test-case'
fi

LAVA_RAISE='echo # '
if [ -n `which lava-test-raise || true` ]; then
    LAVA_RAISE='lava-test-raise'
fi

# Only intended for devices using ADB over USB

which adb

ls -lR /dev/bus/usb

# start adb and stop the daemon start message from appearing in $result
adb get-serialno || true # start daemon if not yet running.
sleep 1
adb get-serialno || true # start daemon if not yet running.
result=`adb get-serialno 2>&1 | tail -n1`
if [ "$result" = "unknown" ]; then
    echo "ERROR: adb get-serialno returned" $result
    ${LAVA_RAISE} "Failed to get serial number with adb"
    exit 1
elif [ "$result" = "error: no devices/emulators found" ]; then
    ${LAVA_RAISE} $result
    exit 2
fi

${LAVA} adb-serialno --result pass
export ANDROID_SERIAL="${result}"
echo "$result" > adb-connection.txt

