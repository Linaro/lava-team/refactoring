#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

# pylint: disable=superfluous-parens,missing-docstring

import os
import subprocess


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def _which_check(path, match):
    """
    Simple replacement for the `which` command found on
    Debian based systems. Allows ordinary users to query
    the PATH used at runtime.
    """
    paths = os.environ['PATH'].split(':')
    if os.getuid() != 0:
        # avoid sudo - it may ask for a password on developer systems.
        paths.extend(['/usr/local/sbin', '/usr/sbin', '/sbin'])
    for dirname in paths:
        candidate = os.path.join(dirname, path)
        if match(candidate):
            return candidate
    return None


def main(args):  # pylint: disable=unused-argument
    values = []
    with open('/tmp/aep-data.log', 'r') as log:
        data = log.read()
    for line in data.split('\n'):
        value_list = line.split(' ')
        try:
            if not isfloat(value_list[0]):
                if 'ARMED' not in value_list[0]:
                    continue
                values.append(float(value_list[2]))
            elif len(value_list) >= 3:
                values.append(float(value_list[2]))
        except ValueError as exc:
            print("possible malformed line in output: %s %s", value_list[2], exc)
    if len(values) == 0:
        if _which_check(path='lava-test-raise', match=os.path.isfile):
            subprocess.check_call(['lava-test-raise', 'AEP setup failed'])
        else:
            print("AEP setup failed")
        return 1
    average = sum(values) / float(len(values))
    if _which_check(path='lava-test-case', match=os.path.isfile):
        subprocess.check_call([
            'lava-test-case', 'probe-results', '--result', 'pass',
            '--measurement', str(average), '--units', 'volts'])
    else:
        print("Average: %f volts" % average)
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
