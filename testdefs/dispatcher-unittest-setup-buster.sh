#!/bin/sh

# dispatcher unit test setup

. ./testdefs/lava-common

export DEBIAN_FRONTEND=noninteractive
unset LANG
unset LANGUAGE
command 'update-apt' "apt-get update -q"
command 'install-base' "apt-get -q -y install -o Dpkg::Options::=--force-confold git bc pep8 telnet python3-testscenarios u-boot-tools nfs-kernel-server tftpd-hpa  libguestfs-tools rpcbind lxc python3-pyudev python3-jinja2 python3-voluptuous"
command 'install-qemu' "apt-get -q -y install --no-install-recommends qemu-system-x86"
# fix up the hosts file before installing apache
echo 127.0.0.1   localhost `hostname` >> /etc/hosts
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
command 'apache' "apt-get -q -y install apache2"
command "get-staging-key" 'wget http://images.validation.linaro.org/staging-repo/staging-repo.key.asc'
apt-key add staging-repo.key.asc
echo "deb http://mirror.bytemark.co.uk/debian stretch-backports main" > /etc/apt/sources.list.d/lava-backports.list
echo "deb http://images.validation.linaro.org/staging-repo stretch-backports main" > /etc/apt/sources.list.d/lava-staging.list
command 'update-apt-with-new-key' "apt-get update -q"
command 'install-dispatcher' "apt-get -q -y install -o Dpkg::Options::=--force-confnew lava-dispatcher"
command 'clone-lava' "git clone -q git://git.linaro.org/lava/lava.git"
cd lava
# allow rpcinfo to find a useful service
mkdir -p /var/lib/lava/dispatcher/tmp
cp etc/lava-dispatcher-nfs.exports /etc/exports.d/
exportfs -fa
testcase 'restart' "service nfs-kernel-server restart"
testcase 'nfs' "service nfs-kernel-server status"
testcase 'rpcinfo' "rpcinfo -u 127.0.0.1 nfs 3"
