#!/bin/sh

# dispatcher unit test setup

. ./testdefs/lava-common

command 'update-apt' "apt-get update -q"
export DEBIAN_FRONTEND=noninteractive
command 'install-base' "apt-get -q -y install -o Dpkg::Options::=--force-confold git wget gnupg pep8 postgresql-9.6 python3-django python3-django-tables2 python3-django-restricted-resource python3-django-testscenarios qemu-system-x86 lava-coordinator locales lxc"

unset LANG
command 'apt_upgrade' "apt-get -q -y upgrade"
command 'apache' "apt -q -y install apache2"


command "get-staging-key" 'wget http://images.validation.linaro.org/staging-repo/staging-repo.key.asc'
apt-key add staging-repo.key.asc
echo "deb http://mirror.bytemark.co.uk/debian stretch-backports main" > /etc/apt/sources.list.d/lava-backports.list
echo "deb http://images.validation.linaro.org/staging-repo stretch-backports main" > /etc/apt/sources.list.d/lava-staging.list
command 'update-apt-with-new-key' "apt-get update -q"
command 'install-backports' "apt-get -q -y -t stretch-backports install python3-django-auth-ldap python3-django-testscenarios"
command 'install-devscripts' "apt-get -q -y --no-install-recommends install devscripts"
command 'install-server' "apt-get -q -y install -o Dpkg::Options::=--force-confnew lava-server"
apt-get clean
command 'clone-lava' "git clone -q git://git.linaro.org/lava/lava.git"
