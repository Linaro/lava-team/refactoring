#!/bin/sh

# dispatcher unit test setup

. ./testdefs/lava-common

unset LANG
unset LANGUAGE
command 'update-apt' "apt-get update -q"
export DEBIAN_FRONTEND=noninteractive
command 'install-base' "apt-get -q -y install -o Dpkg::Options::=--force-confold git wget gnupg pep8 postgresql python3-django python3-django-tables2 python3-django-restricted-resource python3-django-testscenarios lava-coordinator locales lxc"
command 'install-qemu' "apt-get -q -y install --no-install-recommends qemu-system-x86"
command 'apt_upgrade' "apt-get -q -y upgrade"
command 'apache' "apt -q -y install apache2"
command 'install-devscripts' "apt-get -q -y --no-install-recommends install devscripts"
command "get-staging-key" 'wget http://images.validation.linaro.org/staging-repo/staging-repo.key.asc'
apt-key add staging-repo.key.asc
echo "deb http://mirror.bytemark.co.uk/debian stretch-backports main" > /etc/apt/sources.list.d/lava-backports.list
echo "deb http://images.validation.linaro.org/staging-repo stretch-backports main" > /etc/apt/sources.list.d/lava-staging.list
command 'update-apt-with-new-key' "apt-get update -q"
command 'install-server' "apt-get -q -y install -o Dpkg::Options::=--force-confnew lava-server"
apt-get clean
command 'clone-lava' "git clone -q git://git.linaro.org/lava/lava.git"
